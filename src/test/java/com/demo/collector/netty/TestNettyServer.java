package com.demo.collector.netty;

import com.demo.collector.util.caster.TypeCaster;
import com.demo.collector.util.properties.ServerPortProperties;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;

/**
 * @author 沈伟峰
 */
@SpringBootTest
public class TestNettyServer {

    @Autowired
    private ServerPortProperties portProperties;

    @Test
    public void tempTest() {
        try {
            Socket socket = new Socket("localhost", portProperties.getPort("temp"));
            OutputStream out = socket.getOutputStream();
            long time = System.currentTimeMillis();
            double temp = 400;
            out.write(TypeCaster.longToBytes(time));
            out.write(TypeCaster.doubleToBytes(temp));
            System.out.println(Arrays.toString(TypeCaster.longToBytes(time)));
            System.out.println(Arrays.toString(TypeCaster.doubleToBytes(temp)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void humiTest() {
        try {
            Socket socket = new Socket("localhost", portProperties.getPort("humi"));
            OutputStream out = socket.getOutputStream();
            long time = System.currentTimeMillis();
            double humi = 36.66;
            out.write(TypeCaster.longToBytes(time));
            out.write(TypeCaster.doubleToBytes(humi));
            System.out.println(Arrays.toString(TypeCaster.longToBytes(time)));
            System.out.println(Arrays.toString(TypeCaster.doubleToBytes(humi)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void airTest() {
        try {
            Socket socket = new Socket("localhost", portProperties.getPort("air"));
            OutputStream out = socket.getOutputStream();
            long time = System.currentTimeMillis();
            double air = 123.4;
            out.write(TypeCaster.longToBytes(time));
            out.write(TypeCaster.doubleToBytes(air));
            System.out.println(Arrays.toString(TypeCaster.longToBytes(time)));
            System.out.println(Arrays.toString(TypeCaster.doubleToBytes(air)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
