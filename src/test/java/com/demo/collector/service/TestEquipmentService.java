package com.demo.collector.service;

import com.demo.collector.dto.AirQuality;
import com.demo.collector.dto.Humidity;
import com.demo.collector.dto.Temperature;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * @author 沈伟峰
 */
@SpringBootTest
public class TestEquipmentService {

    @Autowired
    private EquipmentService equipmentService;

    @Test
    public void saveTempTest() {
        Random random = new Random(System.currentTimeMillis());
        List<Temperature> list = new ArrayList<>();
        for (int i = 0; i < 5; ++i)
            list.add(new Temperature(System.currentTimeMillis() + random.nextInt(100), random.nextDouble() * 200 + 200));
        int count = equipmentService.saveTemp(1, list.toArray(Temperature[]::new));
        Assertions.assertEquals(list.size(), count);
    }

    @Test
    public void saveHumiTest() {
        Random random = new Random(System.currentTimeMillis());
        List<Humidity> list = new ArrayList<>();
        for (int i = 0; i < 5; ++i)
            list.add(new Humidity(System.currentTimeMillis() + random.nextInt(100), random.nextDouble() * 90 + 10));
        int count = equipmentService.saveHumi(1, list.toArray(Humidity[]::new));
        Assertions.assertEquals(list.size(), count);
    }

    @Test
    public void saveAirTest() {
        Random random = new Random(System.currentTimeMillis());
        List<AirQuality> list = new ArrayList<>();
        for (int i = 0; i < 5; ++i)
            list.add(new AirQuality(System.currentTimeMillis() + random.nextInt(100), random.nextDouble() * 200 + 50));
        int count = equipmentService.saveAir(1, list.toArray(AirQuality[]::new));
        Assertions.assertEquals(list.size(), count);
    }
}
