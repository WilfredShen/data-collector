package com.demo.collector.enity;

import lombok.Data;

@Data
public class HealthReport {
    String symptom;
    String advice;
    double score;
   public  HealthReport(String ans){
       double sum=40;
        for(int i=0;i<8;++i){
            sum-=ans.charAt(i)-'0';
        }
        sum=sum/40*10;
        score=Math.ceil(sum)*10;
        symptom="";
        advice="";
        if(ans.charAt(6)-'0'>3|| ans.charAt(7)-'0'>3|| ans.charAt(1)-'0'>3|| ans.charAt(2)-'0'>3|| ans.charAt(3)-'0'>3||ans.charAt(4)-'0'>3){
            symptom+="肺部存在问题\n";
            advice+="不要抽烟喝酒，不要吃辛辣刺激性的食物\n不要再有污染的环境下呆着，不要在矿山煤矿工作的\n尽量多喝水，多吃点青菜水果，每天多做一些有肺活量的运动。";
        }
       if(ans.charAt(7)-'0'>3|| ans.charAt(6)-'0'>3){
           symptom+="精神不振\n";
           advice+="适当的喝一些党参汤，而且也可以吃一些酸枣仁来进行调整\n在平时的时候要注意自己的病情护理，而且要注意多休息\n在睡之前的时候也可以做一些运动能够促进睡眠。";
       }
    }

    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    public String getAdvice() {
        return advice;
    }

    public void setAdvice(String advice) {
        this.advice = advice;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
