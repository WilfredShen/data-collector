package com.demo.collector.enity;

import lombok.Data;

import java.sql.Timestamp;
@Data
public class Question {
    private Integer userId;
    private String answer;
    private Timestamp time;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }
}
