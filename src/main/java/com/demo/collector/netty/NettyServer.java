package com.demo.collector.netty;

/**
 * @author 沈伟峰
 */
public interface NettyServer {

    void start() throws Exception;

    void close() throws Exception;
}
