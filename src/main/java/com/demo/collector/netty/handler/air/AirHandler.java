package com.demo.collector.netty.handler.air;

import com.demo.collector.dto.AirQuality;
import com.demo.collector.mq.KafkaUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * @author 沈伟峰
 */
@Slf4j
public class AirHandler extends SimpleChannelInboundHandler<AirQuality> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, AirQuality airQuality) {
        log.debug("timestamp: {}, air quality: {}", airQuality.getTime(), airQuality.getAirQuality());
        KafkaUtil.sendMessage(KafkaUtil.TOPIC_AIR, airQuality);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
    }
}
