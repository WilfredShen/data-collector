package com.demo.collector.netty.handler.temp;

import com.demo.collector.dto.Temperature;
import com.demo.collector.util.caster.TypeCaster;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author 沈伟峰
 */
@Slf4j
public class TempDecoder extends MessageToMessageDecoder<ByteBuf> {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) {
        long time = msg.readLongLE();
        double temp = msg.readDoubleLE();
        out.add(new Temperature(time, temp));
    }
}
