package com.demo.collector.netty.handler.air;

import com.demo.collector.dto.AirQuality;
import com.demo.collector.dto.Humidity;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author 沈伟峰
 */
@Slf4j
public class AirDecoder extends MessageToMessageDecoder<ByteBuf> {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) {
        long time = msg.readLongLE();
        double air = msg.readDoubleLE();
        out.add(new AirQuality(time, air));
    }
}
