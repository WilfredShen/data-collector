package com.demo.collector.netty.handler.humi;

import com.demo.collector.dto.Humidity;
import com.demo.collector.mq.KafkaUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * @author 沈伟峰
 */
@Slf4j
public class HumiHandler extends SimpleChannelInboundHandler<Humidity> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Humidity humidity) {
        log.debug("timestamp: {}, humidity: {}", humidity.getTime(), humidity.getHumidity());
        KafkaUtil.sendMessage(KafkaUtil.TOPIC_HUMI, humidity);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
    }
}
