package com.demo.collector.netty.handler.humi;

import com.demo.collector.dto.Humidity;
import com.demo.collector.dto.Temperature;
import com.demo.collector.util.caster.TypeCaster;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageDecoder;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * @author 沈伟峰
 */
@Slf4j
public class HumiDecoder extends MessageToMessageDecoder<ByteBuf> {

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) {
        long time = msg.readLongLE();
        double humi = msg.readDoubleLE();
        out.add(new Humidity(time, humi));
    }
}
