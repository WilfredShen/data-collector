package com.demo.collector.netty.handler.temp;

import com.demo.collector.dto.Temperature;
import com.demo.collector.mq.KafkaUtil;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import lombok.extern.slf4j.Slf4j;

/**
 * @author 沈伟峰
 */
@Slf4j
public class TempHandler extends SimpleChannelInboundHandler<Temperature> {

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, Temperature temperature) {
        log.debug("timestamp: {}, temperature: {}", temperature.getTime(), temperature.getTemperature());
        KafkaUtil.sendMessage(KafkaUtil.TOPIC_TEMP, temperature);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
    }
}
