package com.demo.collector.netty.initializer;

import com.demo.collector.netty.handler.air.AirDecoder;
import com.demo.collector.netty.handler.air.AirHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.FixedLengthFrameDecoder;

/**
 * @author 沈伟峰
 */
public class AirServerInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel socketChannel) {
        ChannelPipeline pipeline = socketChannel.pipeline();
        pipeline.addLast(new FixedLengthFrameDecoder(16));
        pipeline.addLast(new AirDecoder());
        pipeline.addLast(new AirHandler());
    }
}
