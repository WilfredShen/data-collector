package com.demo.collector.netty.initializer;

import com.demo.collector.netty.handler.humi.HumiDecoder;
import com.demo.collector.netty.handler.humi.HumiHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.FixedLengthFrameDecoder;

/**
 * @author 沈伟峰
 */
public class HumiServerInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel socketChannel) {
        ChannelPipeline pipeline = socketChannel.pipeline();
        pipeline.addLast(new FixedLengthFrameDecoder(16));
        pipeline.addLast(new HumiDecoder());
        pipeline.addLast(new HumiHandler());
    }
}
