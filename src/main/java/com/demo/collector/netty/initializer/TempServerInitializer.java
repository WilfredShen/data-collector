package com.demo.collector.netty.initializer;

import com.demo.collector.netty.handler.temp.TempDecoder;
import com.demo.collector.netty.handler.temp.TempHandler;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.FixedLengthFrameDecoder;

/**
 * @author 沈伟峰
 */
public class TempServerInitializer extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel socketChannel) {
        ChannelPipeline pipeline = socketChannel.pipeline();
        pipeline.addLast(new FixedLengthFrameDecoder(16));
        pipeline.addLast(new TempDecoder());
        pipeline.addLast(new TempHandler());
    }
}
