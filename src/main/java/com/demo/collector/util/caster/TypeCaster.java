package com.demo.collector.util.caster;

/**
 * @author 沈伟峰
 */
public class TypeCaster {

    public static byte[] longToBytes(long l) {
        byte[] bytes = new byte[8];
        for (int i = 0; i < 8; i++)
            bytes[i] = (byte) ((l >> (i << 3)) & 0xff);
        return bytes;
    }

    public static byte[] doubleToBytes(double d) {
        long l = Double.doubleToRawLongBits(d);
        byte[] bytes = new byte[8];
        for (int i = 0; i < 8; i++)
            bytes[i] = (byte) ((l >> (i << 3)) & 0xff);
        return bytes;
    }

    public static long bytesToLong(byte[] bytes) {
        long l = 0;
        for (int i = 0; i < 8; i++)
            l |= ((long) (bytes[i] & 0xff)) << (i << 3);
        return l;
    }

    public static double bytesToDouble(byte[] bytes) {
        long l = 0;
        for (int i = 0; i < 8; i++)
            l |= ((long) (bytes[i] & 0xff)) << (i << 3);
        return Double.longBitsToDouble(l);
    }
}
