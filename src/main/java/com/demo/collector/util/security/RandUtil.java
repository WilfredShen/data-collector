package com.demo.collector.util.security;

/**
 * @author 沈伟峰
 */
public class RandUtil {

    private static final String SALT_CHARSET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

    /**
     * 随机生成8个字母的盐值
     *
     * @return 8个字母的盐值
     */
    public static String getSalt() {

        StringBuilder builder = new StringBuilder();
        int len = SALT_CHARSET.length();
        for (int i = 0; i < 8; i++) {
            int index = (int) (Math.random() * (len) - 1);
            builder.append(SALT_CHARSET.charAt(index));
        }
        return builder.toString();
    }
}
