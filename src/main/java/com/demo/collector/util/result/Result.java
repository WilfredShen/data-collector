package com.demo.collector.util.result;

/**
 * @author 沈伟峰
 */
public interface Result {

    int status();

    String message();

    Object data();
}
