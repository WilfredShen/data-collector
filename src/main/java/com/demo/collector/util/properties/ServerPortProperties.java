package com.demo.collector.util.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * @author 沈伟峰
 */
@ConfigurationProperties(prefix = "netty-servers")
@Data
public class ServerPortProperties {

    private Map<String, Integer> ports;

    public int getPort(String name) throws Exception {
        if (!ports.containsKey(name))
            throw new Exception("server not found.");
        return ports.get(name);
    }
}
