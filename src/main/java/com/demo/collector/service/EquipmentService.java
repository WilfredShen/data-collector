package com.demo.collector.service;

import com.demo.collector.dto.AirQuality;
import com.demo.collector.dto.Humidity;
import com.demo.collector.dto.Temperature;

/**
 * @author 沈伟峰
 */
public interface EquipmentService {

    int saveTemp(int userId, Temperature... temps);

    int saveHumi(int userId, Humidity... humis);

    int saveAir(int userId, AirQuality... airs);
}
