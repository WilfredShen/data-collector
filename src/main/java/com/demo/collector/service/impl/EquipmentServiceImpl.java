package com.demo.collector.service.impl;

import com.demo.collector.dto.AirQuality;
import com.demo.collector.dto.Humidity;
import com.demo.collector.dto.Temperature;
import com.demo.collector.mapper.EquipmentMapper;
import com.demo.collector.service.EquipmentService;
import com.demo.collector.util.security.TokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;

/**
 * @author 沈伟峰
 */
@Service
@Slf4j
public class EquipmentServiceImpl implements EquipmentService {

    @Autowired
    private EquipmentMapper equipmentMapper;

    @Override
    public int saveTemp(int userId, Temperature... temps) {
        try {
            return equipmentMapper.saveTemp(userId, Arrays.asList(temps));
        } catch (Exception e) {
            log.error(e.toString());
            return -1;
        }
    }

    @Override
    public int saveHumi(int userId, Humidity... humis) {
        try {
            return equipmentMapper.saveHumi(userId, Arrays.asList(humis));
        } catch (Exception e) {
            log.error(e.toString());
            return -1;
        }
    }

    @Override
    public int saveAir(int userId, AirQuality... airs) {
        try {
            return equipmentMapper.saveAir(userId, Arrays.asList(airs));
        } catch (Exception e) {
            log.error(e.toString());
            return -1;
        }
    }
}
