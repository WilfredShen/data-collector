package com.demo.collector.mq;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

/**
 * @author 沈伟峰
 */
@Slf4j
@Component
public class KafkaUtil {
    /**
     * 温度
     */
    public static final String TOPIC_TEMP = "topic.temp";

    /**
     * 湿度
     */
    public static final String TOPIC_HUMI = "topic.humi";

    /**
     * 空气质量
     */
    public static final String TOPIC_AIR = "topic.air";

    private static KafkaTemplate<String, Object> kafkaTemplate;

    @Autowired
    public KafkaUtil(KafkaTemplate<String, Object> kafkaTemplate) {
        KafkaUtil.kafkaTemplate = kafkaTemplate;
    }

    public static void sendMessage(String topic, Object message) {
        // TODO
    }

}
