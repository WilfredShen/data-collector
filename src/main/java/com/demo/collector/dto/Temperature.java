package com.demo.collector.dto;

import lombok.Data;

import java.sql.Timestamp;

/**
 * @author 沈伟峰
 */
@Data
public class Temperature {

    private Timestamp time;
    private double temperature;

    public Temperature(long time, double temperature) {
        this.time = new Timestamp(time);
        this.temperature = temperature;
    }

    public Temperature(Timestamp time, double temperature) {
        this.time = time;
        this.temperature = temperature;
    }
}
