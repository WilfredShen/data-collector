package com.demo.collector.dto;

import lombok.Data;

import java.sql.Timestamp;

/**
 * @author 沈伟峰
 */
@Data
public class Humidity {

    private Timestamp time;
    private double humidity;

    public Humidity(long time, double humidity) {
        this.time = new Timestamp(time);
        this.humidity = humidity;
    }

    public Humidity(Timestamp time, double humidity) {
        this.time = time;
        this.humidity = humidity;
    }
}
