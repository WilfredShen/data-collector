package com.demo.collector.dto;

import lombok.Data;

import java.sql.Timestamp;

/**
 * @author 沈伟峰
 */
@Data
public class AirQuality {

    private Timestamp time;
    private double airQuality;

    public AirQuality(long time, double airQuality) {
        this.time = new Timestamp(time);
        this.airQuality = airQuality;
    }

    public AirQuality(Timestamp time, double airQuality) {
        this.time = time;
        this.airQuality = airQuality;
    }
}
