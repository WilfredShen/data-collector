package com.demo.collector.mapper;

import com.demo.collector.dto.AirQuality;
import com.demo.collector.dto.Humidity;
import com.demo.collector.dto.Temperature;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author 沈伟峰
 */
@Mapper
public interface EquipmentMapper {

    int saveTemp(int userId, List<Temperature> list) throws Exception;

    int saveHumi(int userId, List<Humidity> list) throws Exception;

    int saveAir(int userId, List<AirQuality> list) throws Exception;
}
