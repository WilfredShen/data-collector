package com.demo.collector.mapper;

import com.demo.collector.enity.Question;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
@Mapper
public interface QuesionMapper {
    void addQuestion(Question question);

    List<Question> listQuestion(@Param("userId")int id);
}
