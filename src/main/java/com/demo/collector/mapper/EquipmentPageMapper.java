package com.demo.collector.mapper;

import com.demo.collector.enity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface EquipmentPageMapper {
    boolean getTempState(@Param("userId") int id);

    boolean getAirState(@Param("userId") int id);

    boolean getHumiState(@Param("userId") int id);

    double getTemp(@Param("userId") int id);

    double getAir(@Param("userId") int id);

    double getHumi(@Param("userId") int id);
}
