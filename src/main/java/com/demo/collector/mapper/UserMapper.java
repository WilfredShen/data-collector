package com.demo.collector.mapper;

import com.demo.collector.enity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;
@Mapper
public interface UserMapper {

    User findUserById(@Param("userId") int id);

    void updateUserDetail(User user);
}
