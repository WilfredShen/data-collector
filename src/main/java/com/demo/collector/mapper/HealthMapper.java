package com.demo.collector.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
@Mapper
public interface HealthMapper {
    String getFinalQuestionById(@Param("userId")int id);

}
