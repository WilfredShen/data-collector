package com.demo.collector.mapper;

import com.demo.collector.enity.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.Map;

/**
 * @author 沈伟峰
 */
@Mapper
public interface LoginMapper {
    String getSalt(@Param("username") String username);

    int getId(@Param("username") String username);

    int checkPassword(@Param("username") String username, @Param("password") String password);

    void addUser(User user);
}
