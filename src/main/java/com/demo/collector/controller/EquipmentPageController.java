package com.demo.collector.controller;

import com.alibaba.fastjson.JSONObject;
import com.demo.collector.enity.User;
import com.demo.collector.mapper.EquipmentMapper;

import com.demo.collector.mapper.EquipmentPageMapper;
import com.demo.collector.util.result.JsonResult;
import com.demo.collector.util.security.TokenUtil;
import org.apache.ibatis.jdbc.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@CrossOrigin
public class EquipmentPageController {
    @Autowired
private EquipmentPageMapper equipmentPageMapper;
    @CrossOrigin
    @RequestMapping(value = "/showEquipment", method = RequestMethod.POST, consumes = "application/json; charset=UTF-8")
    public JsonResult showUserDetail(@RequestBody String jsonParamStr) {
        JSONObject jsonObject = JSONObject.parseObject(jsonParamStr);
        //String token = jsonObject.getString("token");

        int id= 1;//TokenUtil.parseToken(token).get("userId").asInt();
        boolean tempState=equipmentPageMapper.getTempState(id);
        JSONObject json=new JSONObject();
        json.put("type","temp");
        json.put("tempState",tempState);
        if(tempState){
            json.put("temp",equipmentPageMapper.getTemp(id));
        }else{
            json.put("temp",null);
        }
        boolean airState=equipmentPageMapper.getAirState(id);
        json.put("type","air");
        json.put("airState",airState);
        if(airState){
            json.put("air",equipmentPageMapper.getAir(id));
        }else{
            json.put("air",null);
        }
        boolean humiState=equipmentPageMapper.getHumiState(id);
        json.put("type","humi");
        json.put("humiState",humiState);
        if(humiState){
            json.put("humi",equipmentPageMapper.getHumi(id));
        }else {
            json.put("humi",null);
        }
      //  return null;
  //      System.out.println(json);
        return JsonResult.Builder.builder().data(json).build();
    }

}
