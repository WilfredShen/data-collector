package com.demo.collector.controller;

import com.alibaba.fastjson.JSONObject;
import com.demo.collector.enity.Question;
import com.demo.collector.mapper.QuesionMapper;
import com.demo.collector.util.result.JsonResult;
import com.demo.collector.util.security.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;
@RestController
@CrossOrigin
public class QuesionController {
    @Autowired
    private QuesionMapper quesionMapper;
    @CrossOrigin
    @RequestMapping(value = "/addQuestion", method = RequestMethod.POST, consumes = "application/json")
    public JsonResult addQuestion(@RequestBody String jsonParamStr) {
        JSONObject jsonObject = JSONObject.parseObject(jsonParamStr);
       // String token = jsonObject.getString("token");

        int id=4; //TokenUtil.parseToken(token).get("userId").asInt();
        Question question =new Question();
        question.setUserId(id);

        question.setAnswer(jsonObject.getString("answers"));

        //question.setTime(Timestamp.valueOf(System.currentTimeMillis()));

        quesionMapper.addQuestion(question);

        return JsonResult.Builder.success("quesion commitSuccess","");
    }

    @CrossOrigin
    @RequestMapping(value = "/listQuestion", method = RequestMethod.POST, consumes = "application/json")
    public JsonResult listQuestion(@RequestBody String jsonParamStr) {
        JSONObject jsonObject = JSONObject.parseObject(jsonParamStr);
        String token = jsonObject.getString("token");

        int id= TokenUtil.parseToken(token).get("userId").asInt();
        List<Question> quelist=quesionMapper.listQuestion(id);

        return JsonResult.Builder.builder().data(quelist).build();
    }
}
