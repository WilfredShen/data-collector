package com.demo.collector.controller;

import com.alibaba.fastjson.JSONObject;
import com.demo.collector.enity.HealthReport;
import com.demo.collector.mapper.HealthMapper;
import com.demo.collector.mapper.LoginMapper;
import com.demo.collector.util.result.JsonResult;
import com.demo.collector.util.security.HashUtil;
import com.demo.collector.util.security.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
public class HealthController {
    @Autowired
    private HealthMapper healthMapper;
    @CrossOrigin
    @RequestMapping(value = "/health", method = RequestMethod.POST, consumes = "application/json")
    public JsonResult checkPassword(@RequestBody String jsonParamStr) {
        JSONObject jsonObject = JSONObject.parseObject(jsonParamStr);
     //   String token = jsonObject.getString("token");

        int id=4;//TokenUtil.parseToken(token).get("userId").asInt();

        HealthReport healthReport=new HealthReport(healthMapper.getFinalQuestionById(id));

        return JsonResult.Builder.builder().data(healthReport).build();
    }
}
