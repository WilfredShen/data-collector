package com.demo.collector.controller;

import com.alibaba.fastjson.JSONObject;
import com.demo.collector.enity.User;
import com.demo.collector.mapper.UserMapper;
import com.demo.collector.util.result.JsonResult;
import com.demo.collector.util.security.HashUtil;
import com.demo.collector.util.security.RandUtil;
import com.demo.collector.util.security.TokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * @author 沈伟峰
 */
@RestController
@CrossOrigin
public class UserController {
    //病人个人数据
    @Autowired
    private UserMapper userMapper;
    @CrossOrigin
    @RequestMapping(value = "/updateUserDetail", method = RequestMethod.POST, consumes = "application/json; charset=UTF-8")
    public JsonResult updateUserDetail(@RequestBody String jsonParamStr) {
        JSONObject jsonObject = JSONObject.parseObject(jsonParamStr);
        String userName = jsonObject.getString("username");
        String password = jsonObject.getString("password");
        String salt= RandUtil.getSalt();
        User user=new User();
        user.setAge(jsonObject.getInteger("age"));
        user.setUsername(userName);
        user.setPassword(HashUtil.hashPassword(password,salt));
        user.setSalt(salt);


        String token = jsonObject.getString("token");

        int id=TokenUtil.parseToken(token).get("userId").asInt();
        user.setUserId(id);


        String sex = jsonObject.getString("sex");
        if (sex.length() == 2) {
            if (sex.equals("男性")) {
                user.setSex("f");
            } else if (sex.equals("女性")) {
                user.setSex("m");
            }
        } else {
            user.setSex("x");
        }
        userMapper.updateUserDetail(user);
//        Log.changeUserImformation(userMapper.getPatientIdByPhone(patientPhone),"病人");
        return JsonResult.Builder.builder().message("设置成功").build();
    }

    @CrossOrigin
    @RequestMapping(value = "/showUserDetail", method = RequestMethod.POST, consumes = "application/json; charset=UTF-8")
    public JsonResult showUserDetail(@RequestBody String jsonParamStr) {
        JSONObject jsonObject = JSONObject.parseObject(jsonParamStr);
       // String token = jsonObject.getString("token");

        int id=4;//TokenUtil.parseToken(token).get("userId").asInt();
        User user=userMapper.findUserById(id);
        return JsonResult.Builder.builder().data(user).build();
    }


}
